#!/usr/bin/env python

import socket
import time
import re
from enum import Enum
from datetime import datetime

import traceback
import jira
from optparse import OptionParser
from ConfigParser import ConfigParser
from xmlrpclib import Server

from supervisor.options import Options


class IssueStatus(Enum):
    DUPLICATE = 71
    REOPEN = 51
    DONE = 21


class JiraNotifier(object):

    ALERT_INTERVAL = 30*60
    SAMPLE_RATE = 2

    def __init__(self, address=None):
        self.conf = ConfigParser()
        #self.conf.readfp(open(Options().default_configfile()))
        self.conf.readfp(open('/etc/ky.supervisord.conf'))
        self.jira = jira.JIRA(options={'server': address},
                              basic_auth=('KyteraMailer2', 'nbGu8Db3k1'))

        rpc_port = re.findall(r'\d+', self.conf.get('inet_http_server', 'port'))[0]
        self.rpc_client = Server('http://localhost:{port}/RPC2'.format(port=rpc_port))

        self.hostname = socket.gethostname()
        self.services = [section.split(':')[1] for section in
                         self.conf.sections() if 'program' in section]

        self.status_issues_cache = dict.fromkeys(self.services)

    def start_monitoring(self):
        # Initialize service vs issues dictionary
        self.init_service_statues()

        while True:
            try:
                processes = self.rpc_client.supervisor.getAllProcessInfo()
                print 'recieved RPC result from supervisord {0}'.format(str(datetime.now()))
            except Exception:
                traceback.print_exc()
                time.sleep(self.SAMPLE_RATE)
                continue

            for p in processes:
                p_issue = self.status_issues_cache[p['name']]
                issue_status = p_issue.raw['fields']['status']['name']
                if p['statename'] == 'RUNNING' and time.time() - p['start'] > self.ALERT_INTERVAL:
                    if issue_status.upper() != IssueStatus.DONE.name:
                        self.change_issue_status(p_issue, IssueStatus.DONE, p['name'])

                elif p['statename'] == 'STOPPED' and time.time() - p['start'] > self.ALERT_INTERVAL:
                    if issue_status.upper() != 'TO DO':
                        self.change_issue_status(p_issue, IssueStatus.REOPEN, p['name'])

                elif p['statename'] == 'EXITED' or p['statename'] == 'FATAL':
                    if issue_status.upper() != 'TO DO':
                        self.change_issue_status(p_issue, IssueStatus.REOPEN, p['name'])

            time.sleep(self.SAMPLE_RATE)

    def init_service_statues(self):
        for service in self.services:
            generator = '{0}_{1}_status'.format(self.hostname, service)
            issues = self.jira.search_issues('generator="{0}"'.format(generator))
            if not issues:
                summary = 'service {1} crashed on server {0}'.format(self.hostname,
                                                                     service)
                description = 'A service monitoring issue'
                watchers = self.get_service_watchers(service)
                self.status_issues_cache[service] = self.create_jira(watchers, generator, service,
                                                                     summary, description)
                print 'Issue {1} was created for generator {0}'.format(generator, self.status_issues_cache[service].key)
            else:
                self.status_issues_cache[service] = issues[0]
                print 'Found Issue {0} with generator {1}'.format(issues[0].key, generator)

    def get_service_watchers(self, service):
        watchers = self.conf.get('program:{0}'.format(service), 'recipients').split(',')
        return [recp.strip() for recp in watchers]

    def create_jira(self, watchers, generator, service_name, summary,
                    description='', env='int'):
        try:
            fields = {'customfield_10047': [self.hostname], 'customfield_10048': [env],
                      'customfield_10046': [generator], 'assignee': {'name': 'KyteraMailer2'},
                      'customfield_10049': ['supervisor_exception_%s' % service_name]}

            c = self.jira.create_issue(project="AM", summary=summary,
                                       description=description, issuetype={'name': 'Task'},
                                       **fields)

            for watcher in watchers:
                # Oded user registered as admin
                watcher = 'admin' if watcher == 'oded' else watcher
                self.jira.add_watcher(c, watcher)

            return c

        except:
            traceback.print_exc()

    def change_issue_status(self, issue, status, service_name):
        try:
            self.jira.transition_issue(issue, status.value)
        except:
            traceback.print_exc()
        else:
            if status == IssueStatus.DONE:
                self.status_issues_cache[service_name].raw['fields']['status']['name'] = 'DONE'
            else:
                self.status_issues_cache[service_name].raw['fields']['status']['name'] = 'TO DO'


def build_parser():
    parser = OptionParser(usage="Usage: %prog [options]")

    parser.add_option("-m", "--address", default=r"https://kyteratech.atlassian.net",
                      dest="address", help="JIRA Server address")
    return parser


def main():

    parser = build_parser()
    options, system = parser.parse_args()

    notifier = JiraNotifier(address=options.address)
    notifier.start_monitoring()


if __name__ == '__main__':
    main()
